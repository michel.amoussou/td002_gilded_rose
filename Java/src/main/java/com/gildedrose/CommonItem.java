package com.gildedrose;

public class CommonItem extends Item {
    public CommonItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    public void qualityUpdate() {
        if (quality > 0) {
            quality--;
            if (sellIn == 0 && quality > 1) {
                quality--;
            }
        }
    }
}
